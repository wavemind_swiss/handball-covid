class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  layout :layout_by_resource

  # Define layout if user is connected
  def layout_by_resource
    if user_signed_in?
      'application'
    else
      'login'
    end
  end
end

